# Description

Le fichier analogue_S0_S1.nw contient un code qui évalue le facteur de forme entre une première surface noire isotherme et une seconde.

Ce code est sensé servir de point de départ pour le développement d'un code simulant le bilan radiatif de la première surface, échangeant avec la seconde et avec l'environnement (supposé noir et isotherme), sachant que les deux surfaces sont maintenant non-isothermes et réfléchissantes.

# Compilation

bash compilation.bash

# Execution avec les deux spheres

./facteur_de_forme 10000 ../Geometries/sphere1.obj ../Geometries/sphere2.obj

# Visualisation des chemins

Lancer paraview avec la commande suivante :

paraview &

Ouvrir ../Geometries/sphere1.obj et ../Geometries/sphere2.obj, puis ouvrir path.obj
