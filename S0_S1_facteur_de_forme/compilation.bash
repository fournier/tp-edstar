noweave -delay -index -latex analogue_S0_S1.nw > analogue_S0_S1.tex
pdflatex analogue_S0_S1.tex
notangle -Rcompilation.bash analogue_S0_S1.nw > compilation.bash
notangle -Rmain.c analogue_S0_S1.nw > main.c
gcc -c main.c -I $STAR_ENGINE/Star-Engine-0.6.0-GNU-Linux64/include
gcc -o facteur_de_forme main.o -L $STAR_ENGINE/Star-Engine-0.6.0-GNU-Linux64/lib -ls3d -ls3daw -lssp -lm -Wl,-rpath=$STAR_ENGINE/Star-Engine-0.6.0-GNU-Linux64/lib
