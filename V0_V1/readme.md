# Description

Le fichier ccr_V0_V1.nw contient un code dans lequel un chemin conductif est réalisé, partant d'une position donnée au sein d'un objet (au centre de ../Geometries/sphere2.obj), jusqu'à atteindre la frontière, suivi par un chemin radiatif.

Ce code est sensé servir de point de départ pour le développement d'un code simulant la température en un point d'un solide conductif, échangeant par convection avec un fluide isotherme de température connue, et échangeant radiativement avec l'environnement et avec un second solide, lui-même conductif et échangeant avec le fluide, etc.

# Compilation
 
bash compilation.bash

# Execution avec les deux spheres

./temperature 10000 ../Geometries/sphere1.obj ../Geometries/sphere2.obj

# Visualisation des chemins

Lancer paraview avec la commande suivante :

paraview &

Ouvrir ../Geometries/sphere1.obj et ../Geometries/sphere2.obj, puis ouvrir path.obj

Baisser l'opacité pour sphere2.obj de façon à voir les chemins à l'intérieur de la sphere.